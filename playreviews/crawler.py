# -*- coding: utf-8 -*-

import pandas as pd
import requests
from selenium import webdriver
from lxml import etree
import time
import os
import io
import logging

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

class Crawler:
    _MAX_SCROLLS_TIME = 60*60
    _SCROLL_SLEEP = 1.0
    PLAY_APP_URL='https://play.google.com/store/apps/details?id={0}&showAllReviews=true'
    DF_FILENAME='raw/{0}.{1}.json'
    MIN_REVIEWS = 10000

    def __init__(self, app_id, df_filename=DF_FILENAME):
        log.info("Init Crawler")
        self._driver = None
        self.app_id = app_id
        self.df_filename = df_filename

    def _create_webdriver(self):
        log.info("Create Chrome webdriver")
        self._driver = webdriver.Chrome()

    def _quit_webdriver(self):
        log.info("Quit Chrome webdriver")
        if self._driver:
            self._driver.quit()
            self._driver = None

    def get_review_records_from_app(self, min_reviews=MIN_REVIEWS):
        log.info("Get review records from app, min %d", min_reviews)
        end_time = time.time() + self._MAX_SCROLLS_TIME
        for _ in range(min_reviews):
            self._driver.execute_script('window.scrollBy(0, 10000);')
            time.sleep(self._SCROLL_SLEEP)
            review_records = self._driver.find_elements_by_css_selector('div:nth-of-type(1) > div[role="button"] > content > span')
            current_review_records_len = len(review_records)
            log.debug("Got %d records, left time %.2f s", current_review_records_len, end_time - time.time())
            if time.time() >= end_time:
                log.info("Stop collecting reviews (MAX_SCROLL_TIME), got %d records", current_review_records_len)
                break
            if current_review_records_len >= min_reviews:
                log.info("Stop collecting reviews, got %d records", current_review_records_len)
                break
            show_more_buttons = self._driver.find_elements_by_css_selector('div:nth-of-type(2) > div[role="button"] > content > span')
            if show_more_buttons and show_more_buttons[-1].text == 'SHOW MORE':
                show_more_buttons[-1].click()
        return review_records

    def get_page_content(self):
        log.info("Get page content")
        try:
            content = self._driver.execute_script('return document.documentElement.outerHTML;')
        except Exception:
            log.exception("Exception while generating page content")
            time.sleep(5)
            content = self._driver.execute_script('return document.documentElement.outerHTML;')
        return content

    def create_page_tree(self, page_content):
        log.info("Create page tree from page content")
        parser = etree.HTMLParser()
        tree = etree.parse(io.StringIO(page_content), parser)
        return tree

    def get_review_records_from_tree(self, tree):
        log.info("Get review records from tree")
        records = tree.xpath('//div/div[@role="button"]/content/span')
        return records[:-1]

    def get_basic_data_from_reviews(self, review_records):
        log.info("Get basic data from reviews records")
        data = []
        for element in review_records:
            record = element.xpath('../../../../../..')[0]
            e = record.xpath('./div/div/span')[0]
            who = e.text
            e = record.xpath('./div/div/div/span/div/div')[0]
            rated = e.attrib['aria-label']
            e = record.xpath('./div/div/div/span[2]')[0]
            when = e.text
            e = record.xpath('./div/div[2]/div/span/div/content/span/div')[0]
            helpfull = e.text
            e = record.xpath('./div[2]/span')[0]
            short_review = e.text
            e = record.xpath('./div[2]/span[2]')[0]
            full_review = e.text
            part_data = {
                'who': who,
                'rate': rated,
                'when': when,
                'helpfull':helpfull,
                'short_review': short_review,
                'full_review': full_review
                }
            data.append(part_data)
        df = pd.DataFrame(data, columns=['who', 'rate', 'when', 'helpfull', 'short_review', 'full_review'])
        return df

    def save_app_data(self, fname, df):
        log.info("Save application data")
        with open(fname, 'w') as f:
            f.write(df.to_json(orient='records'))

    def get_app_url(self):
        log.info("Get application url")
        return self.PLAY_APP_URL.format(self.app_id)

    def open_app_url(self, app_url):
        log.info("Open application url")
        self._driver.get(app_url)

    def get_app_df_file_name(self):
        log.info("Get application records file name")
        i = 0
        fname = self.df_filename.format(self.app_id, i)
        while os.path.exists(fname):
            i += 1
            fname = self.df_filename.format(self.app_id, i)
        return fname

    def crawl_reviews_from_app_page(self):
        log.info("Crawl application page")
        self._create_webdriver()
        app_url = self.get_app_url()
        self.open_app_url(app_url)
        review_records = self.get_review_records_from_app()
        page_content = self.get_page_content()
        self._quit_webdriver()
        page_tree = self.create_page_tree(page_content)
        review_records = self.get_review_records_from_tree(page_tree)
        df_records = self.get_basic_data_from_reviews(review_records)
        df_fname = self.get_app_df_file_name()
        self.save_app_data(df_fname, df_records)
