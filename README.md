# Google Play Store comments recognition

Could we gain some knowledge about product from user comments?

Could we create program to rank product from user comments?

Could we create get some insights about product from comments,
what bugs they spotted, what features are missing?

## Crawler

### Use Playdrone as source of information about Google Play Store store.

Playdrone application was proof of concept and is not longer maintained.

But this is the only source I have found that is reliable.

Playdrone could be found here https://github.com/nviennot/playdrone .

As authors mention there is no simple way to *crawl* Google Play Store.

Last data crawled by Playdrone was on 2014-10-31 and this data will be used by my review crawler.
Because this is old data, I have made two assumptions of what app use to get reviews:
- downloads over 1M
- still accessible on Play Store

### Crawling

I am selecting 10 apps and from each I am trying to get at least 500 reviews.
I store:
- app metadata
- app page from Google Play
- app reviews

All files are stored in `raw/[app_id].[ext]` files.

Selection of apps is done by pandas `DataFrame.sample` with simple check
if app is still accessible, if not other is selected to match required sample size.

Then I use Selenium and Chrome to open app page and make it show at least 500 reviews,
and collect them.

After that I am just simply store them in plain JSON. Each review record contain:
- "who"
    - "SOME AUTHOR"
- "rate"
    - "Rated 1 stars out of five stars"
- "when"
    - "February 3, 2019"
- "helpfull"
    - "47"
- "short_review"
    - "SOME REVIEW \nFull Review"
- "full_review"
    - ""

### Technical setup

I use Python 3.7.1 with Jupyter as IDE/prototyping tool.

To install all dependencies in virtual environment:
```bash
python -m venv venv
source venv/bin/activate

pip install -r requirements.txt
```

Also as I am using Chrome to open and interact with Google Play Store
so please make sure that matching version of chromedriver to your Chrome
is available on PATH.

To gain data of sample applications run Jupyter notebook *PlayReviews*.

The crawl process is very slow as it use:
- one thread
- real Chrome
- ~~Selenium to get each needed value~~
- probably Google itself has some mechanism to slow down crawlers like this

For example:
```
Get reviews of app com.some.android.scanner
Done app com.some.android.scanner in 326.432578
Get reviews of app com.game.android.app
Done app com.game.android.app in 1120.308827
Get reviews of app com.view.app
Done app com.view.app in 1580.253439
...
```

### Todo

- ~~rewrite Jupyter notebook as python application~~
- split executions to many threads
- use headless browser or not use browser at all
- ~~use LXML or BS4 to get information directly from HTML/DOM, not Selenium~~
- split to many IP/locations to prevent Google recognize this as crawler
- error handling - right now I just skip those apps
- profiling - I should know what time each function consumes

## Recognition

TBD